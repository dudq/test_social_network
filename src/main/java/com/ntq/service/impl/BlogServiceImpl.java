package com.ntq.service.impl;

import com.ntq.common.BlogFilter;
import com.ntq.common.exception.InvalidRequestException;
import com.ntq.models.Blog;
import com.ntq.models.dto.BlogRequestBody;
import com.ntq.repository.BlogRepository;
import com.ntq.service.BlogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BlogServiceImpl implements BlogService {

    @Autowired
    private BlogRepository blogRepository;

    @Override
    public List<Blog> getBlogs() {
        return blogRepository.findAllBy();
    }

    @Override
    public Blog getBlog(Long id) {
        Blog blog = blogRepository.findById(id);
        if (blog == null) {
            throw new InvalidRequestException("Blog is not existed");
        }
        return blog;
    }

    @Override
    public void updateBlog(Blog blog) {
        Blog blogExisting = blogRepository.findById(blog.getId());
        if (blogExisting == null) {
            throw new InvalidRequestException("Blog is not existed");
        }
        blogRepository.save(blog);
    }

    @Override
    public List<Blog> receiveBlogsAnotherUser(BlogRequestBody blogRequestBody) {
        if (BlogFilter.CONTENT.equals(blogRequestBody.getType())) {
            return blogRepository.receiveBlogsByContent(blogRequestBody.getUserId(), blogRequestBody.getCriteria());
        } else if (BlogFilter.NAME.equals(blogRequestBody.getType())) {
            return null;
        } else {
            throw new InvalidRequestException("Blog is not found");
        }
    }

    @Override
    public void updateLikeBlog(Long id) {
        Blog blog = blogRepository.findById(id);
        if (blog == null) {
            throw new InvalidRequestException("Blog is not existed");
        }
        blog.setLiked(blog.getLiked() + 1);
        blogRepository.save(blog);
    }

    @Override
    public void saveBlog(Blog blog) {
        blogRepository.save(blog);
    }

    @Override
    public void deleteBlog(Long id) {
        Blog blog = blogRepository.findById(id);
        if (blog == null) {
            throw new InvalidRequestException("Blog is not existed");
        }
        blogRepository.deleteById(id);
    }

}
