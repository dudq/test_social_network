package com.ntq.service.impl;

import com.ntq.common.exception.InvalidRequestException;
import com.ntq.models.Blog;
import com.ntq.models.User;
import com.ntq.models.dto.UserTopLikeResponse;
import com.ntq.repository.BlogRepository;
import com.ntq.repository.UserRepository;
import com.ntq.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.ArrayList;
import java.util.List;



@Service
public class UserServiceImpl implements UserService {
    private static final int NAME = 0;
    private static final int ID = 1;
    private static final int LIKED = 2;
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private BlogRepository blogRepository;

    @Override
    public List<User> getUsers() {
        return userRepository.findAllBy();
    }

    @Override
    public User getUser(Long id) {
        return userRepository.findById(id);
    }

    @Override
    public void saveUser(User user) {
        userRepository.save(user);
    }

    @Override
    public void removeUser(Long id) {
        User user = userRepository.findById(id);
        if (user == null) {
            throw new EntityNotFoundException("Not found user");
        }
        userRepository.deleteById(id);
    }

    @Override
    public List<Blog> getBlogsByUser(Long id) {
        return null;
    }

    @Override
    public List<UserTopLikeResponse> getTopLike() {
        List<Object> topLike = userRepository.getTopLike();
        int lengthTopLikes = topLike.size();
        Object[] userTopLikeTemp;
        List<UserTopLikeResponse> responses = new ArrayList<>();
        for (int i = 0; i < lengthTopLikes; i++) {
            UserTopLikeResponse userTopLikeResponse = new UserTopLikeResponse();
            userTopLikeTemp = (Object[]) topLike.get(i);
            userTopLikeResponse.setName((String) userTopLikeTemp[NAME]);
            userTopLikeResponse.setId((Long) userTopLikeTemp[ID]);
            userTopLikeResponse.setLiked((Long) userTopLikeTemp[LIKED]);
            responses.add(userTopLikeResponse);
        }
        return responses;
    }
}
