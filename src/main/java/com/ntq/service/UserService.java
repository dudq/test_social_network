package com.ntq.service;

import com.ntq.models.Blog;
import com.ntq.models.User;
import com.ntq.models.dto.UserTopLikeResponse;

import java.util.List;


public interface UserService {
    List<User> getUsers();

    User getUser(Long id);

    void saveUser(User user);

    void removeUser(Long id);

    List<Blog> getBlogsByUser(Long id);

    List<UserTopLikeResponse> getTopLike();
}
