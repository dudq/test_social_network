package com.ntq.models.dto;

import com.ntq.common.BlogFilter;

public class BlogRequestBody {
    private BlogFilter type;
    private String criteria;

    private Long userId;

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public BlogFilter getType() {
        return type;
    }

    public void setType(BlogFilter type) {
        this.type = type;
    }

    public String getCriteria() {
        return criteria;
    }

    public void setCriteria(String criteria) {
        this.criteria = criteria;
    }
}
