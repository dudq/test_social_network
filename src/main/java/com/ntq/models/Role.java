package com.ntq.models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.*;

@Entity
@Table(name = "role")
public class Role implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id", nullable = false)
	private int id;
	
	@Column(name = "name", nullable = false)
	private String name;

	@JsonIgnore
	@OneToMany(mappedBy = "role")
	private Set<UserRole> rolesUser;
	
	public Role() {
	}
	
	public Role(String name) {
		this.name = name;
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set<UserRole> getRolesUser() {
		return rolesUser;
	}

	public void setRolesUser(Set<UserRole> rolesUser) {
		this.rolesUser = rolesUser;
	}
}
