package com.ntq.config;

import com.ntq.models.UserRole;
import com.ntq.repository.UserRoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import com.ntq.models.Role;
import com.ntq.models.User;
import com.ntq.repository.RoleRepository;
import com.ntq.repository.UserRepository;

@Component
public class DataSeedingListener implements ApplicationListener<ContextRefreshedEvent> {
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private RoleRepository roleRepository;
	
	@Autowired 
	private PasswordEncoder passwordEncoder;

	@Autowired
	private UserRoleRepository userRoleRepository;
	
	@Override
	public void onApplicationEvent(ContextRefreshedEvent arg0) {
		// Roles
		if (roleRepository.findByName("ROLE_ADMIN") == null) {
			roleRepository.save(new Role("ROLE_ADMIN"));
		}
		
		if (roleRepository.findByName("ROLE_MEMBER") == null) {
			roleRepository.save(new Role("ROLE_MEMBER"));
		}
		
		// Admin account
		if (userRepository.findAllByName("admin@gmail.com") == null) {
			User admin = new User();
			admin.setName("admin@gmail.com");
			admin.setPassword(passwordEncoder.encode("123456"));
			userRepository.save(admin);
			UserRole userRole = new UserRole();
			userRole.setRole(roleRepository.findByName("ROLE_ADMIN"));
			userRole.setUser(userRepository.findAllByName("admin@gmail.com"));
			userRoleRepository.save(userRole);

		}

		// Member account
		if (userRepository.findAllByName("member@gmail.com") == null) {
			User member = new User();
			member.setName("member@gmail.com");
			member.setPassword(passwordEncoder.encode("123456"));
			userRepository.save(member);
			UserRole userRole = new UserRole();
			userRole.setRole(roleRepository.findByName("ROLE_MEMBER"));
			userRole.setUser(userRepository.findAllByName("member@gmail.com"));
			userRoleRepository.save(userRole);
		}
	}

}
