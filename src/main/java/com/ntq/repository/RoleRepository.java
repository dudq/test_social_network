package com.ntq.repository;

import org.springframework.data.repository.CrudRepository;

import com.ntq.models.Role;

public interface RoleRepository extends CrudRepository<Role, Integer> {

	Role findByName(String name);
	
}
