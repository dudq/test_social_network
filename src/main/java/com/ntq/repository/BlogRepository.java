package com.ntq.repository;

import com.ntq.models.Blog;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;

import java.util.List;

@org.springframework.stereotype.Repository
public interface BlogRepository extends Repository<Blog, Long> {
    List<Blog> findAllBy();

    Blog findById(Long id);

    void save(Blog blog);

    void deleteById(Long id);

    @Query("select b from Blog b where b.user.id=?1")
    List<Blog> findBlogsByUserId(Long id);

    @Query(value = "SELECT * FROM social_network.blogs WHERE  blogs.user_id <> ?1 AND blogs.content like %?2%",nativeQuery = true)
    List<Blog> receiveBlogsByContent(Long id, String content);

    @Query(value = "select * from social_network.blogs inner join social_network.users on blogs.user_id = users.id" +
            " where blogs.user_id <> ?1 and users.name like %?2%", nativeQuery = true)
    List<Blog> receiveBlogsByUserName(Long id, String userName);

}
