package com.ntq.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.ntq.models.User;

import java.util.List;

public interface UserRepository extends CrudRepository<User, Integer> {
	
	User findAllByName(String name);

	List<User> findAllBy();

	User findById(Long id);

	void deleteById(Long id);

	@Query(value = "select u.name as name, u.id as id " +
			"from users u inner join blogs b on u.id = b.user.id GROUP BY u.id, u.name", nativeQuery = true)
	List<Object> getTopLike();


}
