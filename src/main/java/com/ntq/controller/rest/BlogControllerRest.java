package com.ntq.controller.rest;

import com.ntq.models.Blog;
import com.ntq.models.dto.BlogRequestBody;
import com.ntq.service.BlogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/api/blogs")
public class BlogControllerRest {

    @Autowired
    private BlogService blogService;

    @GetMapping
    public ResponseEntity<List<Blog>> listAllBlogs() {
        List<Blog> blogs = blogService.getBlogs();
        if (blogs.isEmpty()) {
            return new ResponseEntity<List<Blog>>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(blogs, HttpStatus.OK);
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<Blog> getBlog(@PathVariable("id") Long id) {
        try {
            Blog blog = blogService.getBlog(id);
            return new ResponseEntity<Blog>(blog, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping
    public ResponseEntity<Void> createBlog(@RequestBody Blog blog) {
        try {
            blogService.saveBlog(blog);
            return new ResponseEntity<>(HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping
    public ResponseEntity<Void> updateBlog(@RequestBody Blog blog) {
        try {
            blogService.updateBlog(blog);
            return new ResponseEntity<>(HttpStatus.UPGRADE_REQUIRED);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteBlog(@PathVariable(value = "id") Long id) {
        try {
            blogService.deleteBlog(id);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/filter")
    public ResponseEntity<List<Blog>> receiveBlogsByFilter(@RequestBody BlogRequestBody blogRequestBody) {
        try {
            List<Blog> blogs = blogService.receiveBlogsAnotherUser(blogRequestBody);
            return new ResponseEntity<>(blogs, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/{id}/like")
    public ResponseEntity<Void> updateLikeBlog(@PathVariable(value = "id") Long id) {
        try {
            blogService.updateLikeBlog(id);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
